#!/usr/bin/env python
# coding: utf-8

from os import listdir
from os.path import isfile, join

import random, sys, json, os
from pyspark import SparkConf
from pyspark.context import SparkContext, SparkConf
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.mllib.classification import SVMWithSGD, SVMModel
# pip install keras tensorflow h5py pillow

from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.models import Model
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import preprocess_input
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
sc = SparkContext.getOrCreate(SparkConf())

sc.setLogLevel('WARN')

#ranger les images dont il faut reconnaitre la race dans ce dossier
dossier="/opt/workspace/projet_2/input/"
#Dossier d'accs aux modèles déjà sauvegardés
dossier_model="/opt/workspace/projet_2/modeles_classe_vs_all/"

#Fonction prediction
def prediction(rdd_test,model):
    return model.predict(rdd_test)

def extract_features(model, image_path):
    img = image.load_img(image_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    features = model.predict(x)
    return features.tolist()[0]


def main():

    base_model = VGG16(weights='imagenet')

    model = Model(inputs=base_model.input, outputs=base_model.get_layer('fc2').output)

    fichiers = [f for f in listdir(dossier) if isfile(join(dossier, f))]
    for image in fichiers:
        if image[-4:] == ".jpg":
            print(image)
            image_path=dossier+image
            features = extract_features(model, image_path)
            with open(image_path + ".json", "w") as out:
                json.dump(features, out)
            rdd_text = sc.textFile(image_path+"*.json")
            rdd=rdd_text.map(
                lambda x: x.strip("[]")).map(
                lambda x: x.split(", "))
            for ele in rdd.collect():
                valeur=ele
            for model_name in listdir(dossier_model): #on liste dossier et fichier du répertoire choisis
                model_path=dossier_model+model_name
                if os.path.isdir(model_path): #si c'est un dossier True
                    model2 = SVMModel.load(sc,model_path)
                    if model2.predict(valeur) == 0:
                        print("correspondance avec model : "+model_name+" pour "+image)

if __name__ == "__main__":
    main()

