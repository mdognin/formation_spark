# coding: utf-8
#!/usr/bin/env python3
#/usr/local/bin/spark-submit /opt/workspace/projet_2/parquet_creation.py

import re
from os import listdir
from os.path import isfile, join
import time
import random, sys, json, os
from pyspark import SparkConf
from pyspark.context import SparkContext, SparkConf
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.mllib.classification import SVMWithSGD, SVMModel
from pyspark.sql import SparkSession, Row, Column
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.models import Model
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import preprocess_input
import tensorflow as tf
import numpy as np

tf.get_logger().setLevel('ERROR')

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
sc = SparkContext.getOrCreate(SparkConf())
spark = SparkSession.builder.appName("app-parquetcreation").master("spark://spark-master:7077").config("spark.executor.memory", "1024m").getOrCreate()
#spark = SparkSession.builder.getOrCreate()
sc.setLogLevel('WARN')

##formate : /opt/workspace/projet_2/images/Abyssinian_102.jpg ==> Abyssinian
def formate(fichier):
    fichier=str(fichier)
    position=fichier.find("_",-9,-1)
    #retirer les derniers caractères
    fichier=fichier[:position]
    #la variable fichier contient le nom de la race
    return fichier

##Cette fonction permet d'extraire les features des images
def extract_features(model, image_path):
    img = image.load_img(image_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    features = model.predict(x)
    return features.tolist()[0]

##Permet d'écrire le fichier parquet en fonction de la liste de features
def fin(liste,num,columns,dossier):
    rdd=sc.parallelize(liste) #conversion en RDD
    rdd=rdd.map(lambda x: (formate(x[0]),x[1])) #On formate le nom du fichier pour obtenir la classe
    df=spark.createDataFrame(rdd).toDF(*columns) #conversion du rdd en df
    df.write.mode('overwrite').parquet(dossier+"global"+str(num)+".parquet", partitionBy="label") #écriture du fichier parquet

def main():
    compt=0
    compt_abs=0
    num=0

    base_model = VGG16(weights='imagenet')

    model = Model(inputs=base_model.input, outputs=base_model.get_layer('fc2').output)
    
    ##Dans cette partie nous créons la liste des images jpg à transformer ensuite en feature
    li_fichier=[]
    for fichier in listdir(dossier_jpg):
        if isfile(join(dossier_jpg, fichier)) and re.match(".*\.jpg",fichier):
            li_fichier.append(fichier)
    long=len(li_fichier)
    li_fichier.sort()
    
    ##Cette boucle for permet de passer en revue tous les fichiers jpg et de les transformer en feature
    li_feature=[]
    columns=['label','features']
    for image in li_fichier:
        image_path = dossier_jpg+image
        feature = extract_features(model, image_path)
        print(compt_abs,"/",long, image)
        li_feature.append([image,feature])
        compt+=1
        compt_abs+=1
        #Dans un premier on crée des fichiers parquet comprenant un nombre déterminé de features
        if compt ==nbr_parquet:
            fin(li_feature,num,columns,dossier_jpg)
            num+=1
            li_feature=[]
            compt=0
    #s'il reste des images qui ne rentrent pas dans le dernier lot de parquet on crée un dernier parquet
    if len(li_feature) !=0:
        fin(li_feature,num,columns,dossier_jpg)
        num+=1

    print("nombre de parquet :",num)
    print("compteur :",compt)
    print("compteur abs :",compt_abs)
    
    compt=0  
    for parquet_nom in listdir(dossier_pqt): #on liste dossier et fichier du répertoire choisis
        parquet_chemin=dossier_jpg+parquet_nom
        if os.path.isdir(parquet_chemin) and re.match(".*\.parquet",parquet_chemin) : #si c'est un dossier True
            if compt==0:
                df1=spark.read.format("parquet").load(parquet_chemin)
            else:
                df1=df1.union(spark.read.format("parquet").load(parquet_chemin))
            compt+=1
    rdd=df1.rdd.map(lambda x: Row(label=x[1],feature=x[0]))
    df=spark.createDataFrame(rdd)
    df.write.mode('overwrite').parquet(dossier_pqt+"global.parquet", partitionBy="label")
    print(rdd.count())
    df=spark.createDataFrame(rdd)
    df.show()
    end = time.time()
    print("compteur rdd final = ", rdd.count())
            
if __name__ == "__main__":
    
    #Dossier ou sont stockés les images JPG
    dossier_jpg="/opt/workspace/projet_2/images/"
    #Dossier ou est stocké le parquet à la fin
    dossier_pqt="/opt/workspace/projet_2/files/"
    #régler le nombre de features par parquet
    nbr_parquet=200
    global_start=time.time()
    main()
    end = time.time()
    print("temps total d'exécution : ", end-global_start)
    input("Ctrl+C pour terminer")