#!/usr/bin/env python
# coding: utf-8
import random, os, sys, re, time, array, csv, os.path
from os import listdir
from os.path import isfile
from pyspark import SparkConf
from pyspark.context import SparkContext, SparkConf
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.feature import StringIndexer, StringIndexerModel
from pyspark.sql import SparkSession, Row
from pyspark.sql.functions import monotonically_increasing_id, col, desc
from pyspark.mllib.classification import SVMWithSGD, SVMModel, LabeledPoint
from pyspark.sql import functions as F


#spark = SparkSession.builder.getOrCreate()
spark = SparkSession.builder.appName("app-projet-2").master("spark://spark-master:7077").config("spark.executor.memory", "1024m").getOrCreate()
sc = SparkContext.getOrCreate(SparkConf())
sc.setLogLevel('WARN')

##La Fonction formatage permet de formater le nom des fichiers JSON pour obtenir la classe : /opt/workspace/projet_2/images/Abyssinian_102.jpg ==> Abyssinian
def formatage(fichier,dossier_temp,type_f):
    fichier=str(fichier)
    if type_f=="json":
        #retirer les premiers caractères
        fichier=fichier[len(dossier_temp)+5:]
        
        position=fichier.find("_",-(10+len(type_f)),-1)
        #retirer les derniers caractères
        fichier=fichier[:position]
        #la variable fichier contient le nom de la classe
    return fichier

### Récupération des données
def load_dataframe(path,type_data,nbr_parquet):
    #soit on utilise les fichiers JSON
    if type_data=="json":
        long_path=len(path)+5
        rdd_text=sc.wholeTextFiles(path+"*.json")
        rdd=rdd_text.map(lambda x: Row(label=x[0],feature=x[1].strip("[]")))\
                    .map(lambda x: Row(label=formatage(x[0],path,"json"),x=x[1].split(", ")))
    #Soit on utilise des fichiers parquet
    elif type_data=="parquet":
        compt=0
        if nbr_parquet ==1:
            df1=spark.read.format("parquet").load(path+"global.parquet")
            rdd=df1.rdd.map(lambda x: Row(label=x[1],feature=x[0]))
    return rdd

###RECUPERER UNE LISTE de classe
# Récupération de toute les classes dans une liste
def f_liste_classe(rdd_input_data):
    rdd_liste_classe=rdd_input_data.map(lambda x: Row(classe = x[0]) )
    rdd_liste_classe=rdd_liste_classe.distinct()
    liste=[]
    for ele in rdd_liste_classe.collect():
        liste.append(ele.classe)
    return liste

### Filtre sur la classe à conserver
def filtre_classe(rdd_input_data,classe,classe_2):
    if classe_2 == "all": #Pour le modèle 1 VS all on met toutes les classes dans le rdd
        rdd_filter_data=rdd_input_data
    else: #pour le modèle 1vs1 on garde seulement les 2 classes concernées
        rdd_filter_data=rdd_input_data.filter(lambda label: label[0] == classe or 
            label[0]== classe_2)
    return rdd_filter_data

### Ajout d'un label indexer ###
def f_label_index(df_filter_data,liste_classe,classe,classe_2):
    liste_model=[]
    liste_model.append(classe) #ajout de la classe à comparer en premier
    if classe_2=="all":
        for ele in liste_classe:
            if ele != classe:
                liste_model.append(ele)
    else:
        liste_model.append(classe_2)
    string_indexer_model=StringIndexerModel.from_labels(liste_model,inputCol="label",outputCol="label_index")
    df_indexer_data = string_indexer_model.transform(df_filter_data)
    
    return df_indexer_data

### Conversion du RDD en Labeled Point
def f_lp_01(classe_ind):
    if classe_ind == 0:
        return 0
    else:
        return 1
    
def conversion_labeled_point(rdd):
    #On en profite pour changer les label index : tout les label_index différents de 0 passent à 1
    rdd=rdd.map(lambda x :Row(x[1],x[2]))
    lp_rdd=rdd.map(lambda row: LabeledPoint(row[0],row[1: ]))
    return lp_rdd
  
###tester le modele avec PREDICT
def prediction(rdd_test,model):
    return model.predict(rdd_test)

def main():
    global_start = time.time()
    
    print("Récupération des données")
    ### Récupération des données dans 1 RDD
    rdd_input_data = load_dataframe(dossier_source,type_data,nbr_parquet).persist()
    
    print("comptage : ",rdd_input_data.count())
    data_end = time.time()
    print("temps de récupération données : ", data_end-global_start)

    df_input_data = spark.createDataFrame(rdd_input_data)
    df_input_data.show()

    #Création de la liste de classes
    #les 2 liste de classe test permettent de balayer tous les cas quand on crée les 666 modèles du "classe VS classe"
    liste_classe_test = []
    liste_classe_test_2 = []
    liste_classe = f_liste_classe(rdd_input_data)

    if (classe1 != "all" and classe2 != "all" ) and one_vs_one==False:
        liste_classe_test.append(classe1)
        liste_classe_test_2.append(classe2)
    elif classe1 != "all" and one_vs_one==False:
        liste_classe_test.append(classe1)
        liste_classe_test_2.append("all")
    elif one_vs_one == False and classe2 == "all":
        liste_classe_test=liste_classe
        liste_classe_test_2.append("all")
    elif one_vs_one == True:
        liste_classe_test=liste_classe.copy()
        liste_classe_test_2=liste_classe.copy()

    compt=0
    
    #On ouvre un fichier csv pour y inscrire tous les résultats
    with open(fichier_csv,mode_ecriture,newline='')as f_csv:
        writer=csv.writer(f_csv)
        writer.writerow(['temps','classe1','classe2','Nbr_erreur','taux_reu','total_element_teste', 'taux_train', 'total_train', 'nbr_iteration_model', 'temps_boucle_train'])
        
        for classe in liste_classe_test: #Boucle for pour balayer toutes les classes
            if one_vs_one == True:
                # On supprime la première valeur de la seconde liste pour être certain de ne pas traiter 2 fois le même couple de classe
                del liste_classe_test_2[0]
            for classe_2 in liste_classe_test_2:
                compt+=1
                
                ### Filtre sur la classe à conserver
                rdd_filter_data = filtre_classe(rdd_input_data,classe,classe_2)

                ###Conversion en DF
                df_filter_data = spark.createDataFrame(rdd_filter_data)

                ### Ajout d'un label indexer ###
                df_indexer_data = f_label_index(df_filter_data,liste_classe,classe,classe_2)

                ###Reconversion en RDD
                rdd_indexer_data = df_indexer_data.rdd.map(lambda x: Row(label=x[0],label_index=f_lp_01(x[2]),feature=x[1])).persist()
                                               
                rdd_test_data, rdd_other_data = rdd_indexer_data.randomSplit([30,70],29)
                
                for val_train in range(train_debut,train_fin,train_pas):
                    train_start = time.time()
                    ###Séparation en données TRAIN et TEST
                    rdd_train_data, rdd_other_2_data = rdd_other_data.randomSplit([val_train,100-val_train],29)
                    
                    for nbr_it in range(it_debut,it_fin,it_pas):

                        ### Construction du modèle
                        dossier_modele=dossier_sauvegarde+classe+"_vs_"+classe_2+"-vtr_"+str(val_train)+"-it_"+str(nbr_it)
                        dossier_modele_inv=dossier_sauvegarde+classe_2+"_vs_"+classe+"-vtr_"+str(val_train)+"-it_"+str(nbr_it)
                        
                        ##Création ou Chargement du model
                        if boucle_it ==False:
                            if os.path.exists(dossier_modele): #on regarde si le modèle existe déjà
                                print("--CHARGEMENT MODEL--")
                                model = SVMModel.load(sc,dossier_modele) #s'il existe on le charge
                            elif os.path.exists(dossier_modele_inv):
                                print("--CHARGEMENT MODEL--")
                                model = SVMModel.load(sc,dossier_modele_inv)
                            else: #sinon on le crée et on le sauvegarde
                                ### Conversion du RDD en Labeled Point
                                lp_rdd_train = conversion_labeled_point(rdd_train_data)
                                ### entrainement du modèle
                                model = SVMWithSGD.train(lp_rdd_train, iterations=nbr_it) 
                                print("--Sauvegarde MODEL--")
                                model.save(sc,dossier_modele)  #Sauvegarde du model
                        else:
                            lp_rdd_train = conversion_labeled_point(rdd_train_data)
                            model = SVMWithSGD.train(lp_rdd_train, iterations=nbr_it) 
                       
                        ###tester le modele avec PREDICT
                        rdd_test_prev = rdd_test_data.map(
                            lambda x : Row(label=x[0],label_index=x[1],pred=prediction(x[2],model))).map(
                            lambda x : Row(label=x[0],label_index=x[1],pred=x[2],diff=abs(x[1]-x[2])) )
                                               
                        somme=0
                        for ele in rdd_test_prev.collect():
                            somme += ele.diff

                        total_test = rdd_test_prev.count()
                        total_train = rdd_train_data.count()
                        train_end = time.time()

                        writer.writerow([train_end-global_start,classe,classe_2,somme,(total_test-somme)/total_test*100, total_test, val_train, total_train, nbr_it, train_end-train_start])
                        print('temps', 'classe1', 'classe2', 'Nbr_erreur', 'taux_reu', 'total_element_teste', 'taux_train', 'total_train', 'nbr_iteration_model', 'temps_boucle_train')
                        print(train_end-global_start,classe,classe_2,somme,(total_test-somme)/total_test*100, total_test, val_train, total_train,  nbr_it, train_end-train_start)

            if nbr_classe!=0 and nbr_classe==compt: #On arrête la boucle si on ne veut pas tester toute les classes
                break

    global_end = time.time()
    print("temps total d'exécution : ", global_end-global_start)
    print("count = ",rdd_input_data.count())


if  __name__ =='__main__':
    
    dossier_source="/opt/workspace/projet_2/files/"
    dossier_sauvegarde="/opt/workspace/projet_2/modeles_classe_vs_all/"
    fichier_csv="/opt/workspace/projet_2/resultat_classe_vs_all.csv"

    ###Paramètres concernant la proportion de données TRAIN/TEST à rentrer pour la boucle for
    #Donner une valeur entre 0 et 100
    train_debut = 50
    train_pas = 10
    train_fin = 51

    ###Modèle
    it_debut = 200
    it_pas = 50
    it_fin = 201
    boucle_it=False
    
    mode_ecriture='w' #a : ajout, w : écraser
    
    type_data="parquet"
    nbr_parquet="1"# 1 ou 0 pour plusieurs

    #nombre de classe à tester
    nbr_classe=0 # 0=all
    #si on veut faire du 1vs1 pour chaque couple
    one_vs_one=False
    #nom de la classe à trier
    classe1="all" #on opeut indiquer une seule classe
    classe2="all"
    
    main()

