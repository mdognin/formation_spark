# Formation_Spark

Ce projet est composé de plusieurs scripts python, à exécuter dans l'ordre :

1- parquet_creation.py : C’est le script pour extraire les features de toutes les images dans un seul fichier parquet

2- projet_2_10-cluster.py : Ceci est le script principal, qui lit les fichier JSON ou parquet, qui entraine et teste les modèles

3-  appli_global_2.py : Ceci est une application bonus qui permet de prédire la race d’une image quelconque de chien ou de chat en fonction des modèles créés précédemment avec le script principal

La présentation powerpoint récapitule les éléments importants du projet.

## But du projet :

- récupérer une bdd d'images de 37 races de chats et de chiens
- extraire de ces images des features et les enregistrer dans des fichiers parquet (ou json)
- entrainer des modèles pour pouvoir prédire l'appartenance à une des 37 races
- tester les modèles avec des images de test

## Moyens techniques

Un cluster Spark est déployé pour 
- s'entraîner à faire du calcul distribuer
- utiliser des algorythmes map/reduce
- manipuler des RDD
- apprendre à enregistrer des informations classées dans des fichiers parquet
