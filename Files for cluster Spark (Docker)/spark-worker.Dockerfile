FROM spark-base

# -- Runtime

ARG spark_worker_web_ui=8081

RUN apt-get update -y && \
	apt-get install python3-pip -y && \
	pip3 install mllib ml-python numpy tensorflow pillow

EXPOSE ${spark_worker_web_ui}
CMD bin/spark-class org.apache.spark.deploy.worker.Worker spark://${SPARK_MASTER_HOST}:${SPARK_MASTER_PORT} >> logs/spark-worker.out