FROM spark-base

# -- Runtime

ARG spark_master_web_ui=8080

RUN apt-get update -y && \
	apt-get install python3-pip -y && \
	pip3 install mllib ml-python numpy

EXPOSE ${spark_master_web_ui} ${SPARK_MASTER_PORT}
CMD bin/spark-class org.apache.spark.deploy.master.Master >> logs/spark-master.out
