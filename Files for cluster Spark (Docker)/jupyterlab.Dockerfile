FROM cluster-base

# -- Layer: JupyterLab

ARG spark_version=3.1.1
ARG jupyterlab_version=3.0.15

RUN apt-get update -y && \
    apt-get install -y python3-pip && \
    pip3 install  wget pyspark==${spark_version} jupyterlab==${jupyterlab_version} numpy tensorflow pillow
    
# -- Runtime

EXPOSE 8889
WORKDIR ${SHARED_WORKSPACE}
CMD jupyter lab --ip=0.0.0.0 --port=8889 --no-browser --allow-root --NotebookApp.token=
